name := """chat-akka"""

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.8",
  "com.typesafe.akka" %% "akka-http-experimental" % "2.4.8",
  "com.typesafe.akka" %% "akka-http-spray-json-experimental" % "2.4.8",
  "org.mongodb.scala" %% "mongo-scala-driver" % "1.1.1",
  "org.scala-lang" % "scala-reflect" % "2.11.8"
)

fork in run := true