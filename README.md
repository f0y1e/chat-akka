# Распределенный (пока нет) чат на websocket'ах #

# API: #

Для подключения к серверу необходимо послать GET-запрос на открытие вебсокета с параметрами login и password.
*Понимаю, что это не комильфо, но давайте представим что у нас тут WSS.*

Пример на Javascript:

```
#!javascript

socket = new WebSocket("ws://<server url>/chat?login=<userId>&password=<password>”); //это зло)
```


**Обозначения:**


```
#!javascript

userId - понятно
pUserId - userId с префиксом “user-”
Пример:
userId = “admin”
pUserId = “user-admin”

roomId - понятно
pRoomId - roomId с префиксом “room-”
Пример:
roomId = “test”
pRoomId = “room-test”

message - JSON вида 
{
“from”: <pUserId>,
“to”: <pUserId или pRoomId>,
“payload”: <текст сообщения>,
“datetime” <время отправки сообщения в unix time>
}
```


## Сообщения, передаваемые клиенту: ##

**Сообщение отправленное пользователю, либо в комнату, в которой состоит пользователь:**

```
#!javascript

{
	“type”: “message”,
	“message”: <message>
}
```


**Список комнат, в которых состоит пользователь:**

```
#!javascript

{
	“type”: “rooms”,
	“rooms”: <массив строк roomId>
}
```



**Список сообщений, относящихся пользователю:**

```
#!javascript

{
	“type”: “messages”,
	“messages”: <массив объектов message, отсортированных по убыванию по 
полю datetime>
}
```


## Сообщения отправляемые клиентом серверу: ##

**Сообщение, отправляемое пользователем другому пользователю или комнате:**

```
#!javascript

{
	“type”: “message”,
	“message”: <message>
}
```


**Присоединиться комнате:**

```
#!javascript

{
	“type”: “join”,
	“roomId”: <roomId>
}
```


**Покинуть комнату:**

```
#!javascript

{
	“type”: “leave”,
	“roomId”: <roomId>
}
```


**Получить список комнат, которым принадлежит пользователь:**

```
#!javascript

{
	“type”: “getrooms”
}
```


**Получить список сообщений, которые принадлежат пользователю:**

```
#!javascript

{
	“type”: “getmessages”
}
```