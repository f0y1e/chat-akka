package com.edu.chat

import java.io.File

import akka.actor.ActorSystem
import com.edu.chat.database.{MessagesDBActor, UsersDBActor}
import com.edu.chat.web.ConnectionHandlerActor
import com.typesafe.config.ConfigFactory

object Launcher extends App {
  val config = ConfigFactory.parseFileAnySyntax(new File(args(0))).resolve()
  val mongoConfig = config.getConfig("mongo")
  val username = mongoConfig.getString("username")
  val password = mongoConfig.getString("password")
  val host = mongoConfig.getString("host")
  val port = mongoConfig.getInt("port")
  val db = mongoConfig.getString("db")

  implicit val actorSystem = ActorSystem.create("chat", config)

  val messagesDBActor = actorSystem.actorOf(MessagesDBActor.props(host, port, username, password, db), "MessagesDBActor")
  val usersDBActor = actorSystem.actorOf(UsersDBActor.props(host, port, username, password, db), "UsersDBActor")
  actorSystem.actorOf(ConnectionHandlerActor.props(usersDBActor, messagesDBActor), "ConnectionHandlerActor")
}
