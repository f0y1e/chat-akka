package com.edu.chat.util

import com.edu.chat.database.UsersDBActor.CheckAuth
import com.edu.chat.web.WebSocketConnectionActor.{ChatMessage, User}
import org.mongodb.scala.bson.collection.immutable.Document
import spray.json._

object Conversions {
  implicit class DocumentConversions(document: Document) {
    def asMessage: ChatMessage = ChatMessage(
      document("from").asString().getValue,
      document("to").asString().getValue,
      document("payload").asString().getValue,
      document("datetime").asDateTime().getValue)

    def asUser: User = User(
      document("userId").asString().getValue,
      document("password").asString().getValue
    )
  }

  object JsonProtocol extends DefaultJsonProtocol {
    implicit val chatMessageFormat = jsonFormat4(ChatMessage)
    implicit val authRequestFormat = jsonFormat2(CheckAuth)
  }
}
