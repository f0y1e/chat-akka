package com.edu.chat.util

import akka.actor.ActorRef
import akka.event.{EventBus, LookupClassification}
import com.edu.chat.web.WebSocketConnectionActor.ChatMessage

class MessageBus extends EventBus with LookupClassification {
  override type Event = ChatMessage
  override type Classifier = String
  override type Subscriber = ActorRef

  override protected def mapSize(): Int = 2048

  override protected def compareSubscribers(a: Subscriber, b: Subscriber): Int = a.compareTo(b)

  override protected def classify(event: Event): String = event.to

  override protected def publish(event: Event, subscriber: Subscriber): Unit = subscriber ! event
}
