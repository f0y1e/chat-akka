package com.edu.chat.web

import akka.NotUsed
import akka.actor.{Actor, ActorNotFound, ActorRef, PoisonPill, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.util.Timeout
import com.edu.chat.database.UsersDBActor.{Authenticated, CheckAuth, NotFound}
import com.edu.chat.util.MessageBus
import com.edu.chat.web.WebSocketConnectionActor.{Incoming, Outgoing}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.io.StdIn

object ConnectionHandlerActor {
  def props(usersDBActor: ActorRef,
            messagesDBActor: ActorRef): Props =
    Props(new ConnectionHandlerActor(usersDBActor, messagesDBActor))
}

class ConnectionHandlerActor(usersDBActor: ActorRef,
                             messagesDBActor: ActorRef) extends Actor {
  private implicit val materializer = ActorMaterializer()
  private implicit val executionContext = context.dispatcher
  private implicit val timeout = Timeout(3.seconds)
  private val messageBus = new MessageBus

  private val webSocketRoute =
    path("chat") {
      parameters("login", "password") { (login, password) =>
        val checkAuth = CheckAuth(login, password)
        val response = usersDBActor ? checkAuth
        val result = Await.result(response, timeout.duration)
        result match {
          case Authenticated =>
            handleWebSocketMessages(createConnection(checkAuth.userId))
          case NotFound =>
            complete("Wrong login/password!")
        }
      }
    }

  private def createConnection(userId: String): Flow[Message, Message, NotUsed] = {
    implicit val timeout = Timeout(3.seconds)
    val connectionActor: ActorRef = try {
      Await.result(
        context.actorSelection(s"Connection-$userId").resolveOne(),
        timeout.duration)
    } catch {
      case _: ActorNotFound =>
        context.actorOf(WebSocketConnectionActor.props(
          userId,
          messagesDBActor,
          usersDBActor,
          messageBus
        ), s"Connection-$userId")
    }

    val incomingMessages: Sink[Message, NotUsed] =
      Flow[Message].filter {
        case message: TextMessage.Strict => true
        case _ => false
      }.map {
        case TextMessage.Strict(text) => Incoming(text)
      }.to(Sink.actorRef[Incoming](connectionActor, PoisonPill))

    val outgoingMessages: Source[Message, NotUsed] =
      Source.actorRef[Outgoing](20, OverflowStrategy.fail)
        .mapMaterializedValue { outActor =>
          connectionActor ! WebSocketConnectionActor.Connected(outActor)
          NotUsed
        }.map {
        outgoing => TextMessage.Strict(outgoing.text)
      }

    Flow.fromSinkAndSource(incomingMessages, outgoingMessages)
  }

  private val bindingFuture = Http(context.system).bindAndHandle(webSocketRoute, "localhost", 8080)

  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine()
  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => context.system.terminate())

  override def receive: Receive = ???
}
