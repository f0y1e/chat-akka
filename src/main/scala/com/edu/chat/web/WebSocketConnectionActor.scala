package com.edu.chat.web

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.edu.chat.database.MessagesDBActor.{GetMessagesOfRoom, GetMessagesOfUser, SaveMessage}
import com.edu.chat.database.UsersDBActor.{GetRoomsOfUser, JoinToRoom, LeaveRoom}
import com.edu.chat.util.Conversions.JsonProtocol._
import com.edu.chat.util.MessageBus
import com.edu.chat.web.WebSocketConnectionActor.{ChatMessage, Connected, Incoming, Outgoing}
import spray.json._

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object WebSocketConnectionActor {
  def props(userId: String,
            messagesDBActor: ActorRef,
            usersDBActor: ActorRef,
            messageBus: MessageBus): Props =
    Props(new WebSocketConnectionActor(
      userId,
      messagesDBActor,
      usersDBActor,
      messageBus))

  case class ChatMessage(from: String, to: String, payload: String, datetime: Long)
  case class User(userId: String, password: String)
  case class Connected(outgoing: ActorRef)
  case class Incoming(text: String)
  case class Outgoing(text: String)
}

class WebSocketConnectionActor(private val userId: String,
                               private val messagesDBActor: ActorRef,
                               private val usersDBActor: ActorRef,
                               private val messageBus: MessageBus) extends Actor with ActorLogging {
  private val UsersPrefix = "user-"
  private val RoomsPrefix = "room-"
  private val clients: mutable.Set[ActorRef] = mutable.SortedSet()
  private implicit val timeout = Timeout(3.seconds)
  private implicit val executionContext = context.dispatcher

  private val rooms: mutable.Set[String] = {
    val response = usersDBActor ? GetRoomsOfUser(userId)
    val result = Await.result(response, timeout.duration)
    mutable.SortedSet(result.asInstanceOf[List[String]]: _*)
  }

  messageBus.subscribe(self, UsersPrefix + userId)
  rooms.foreach(joinToRoom)

  override def receive: Receive = {
    case Connected(clientActorRef) => clients += clientActorRef

    case message: ChatMessage =>
      sendToClients(JsObject(Map(
        "type" -> "message".toJson,
        "message" -> message.toJson
      )).toString)

    case Incoming(text) =>
      val incoming = text.parseJson.asJsObject
      val `type` = incoming.fields("type").asInstanceOf[JsString].value
      `type` match {
        case "message" =>
          sendMessage {
            val message = incoming.fields("message").asJsObject
            val to = message.fields("to").asInstanceOf[JsString].value
            val payload = message.fields("payload").asInstanceOf[JsString].value
            val dateTime = message.fields("datetime").asInstanceOf[JsNumber].value.longValue
            ChatMessage(UsersPrefix + userId, to, payload, dateTime)
          }
        case "join" =>
          joinToRoom(incoming.fields("roomId").asInstanceOf[JsString].value)
        case "leave" =>
          leaveRoom(incoming.fields("roomId").asInstanceOf[JsString].value)
        case "getrooms" =>
          sendRoomsToClients()
        case "getmessages" =>
          sendOldMessagesToClients()
      }
  }

  private def sendToClients(text: String): Unit = {
    clients.foreach {
      _ ! Outgoing(text)
    }
  }

  private def sendMessage(chatMessage: ChatMessage): Unit = {
    messageBus.publish(chatMessage)
    messagesDBActor ! SaveMessage(chatMessage)
  }

  private def joinToRoom(roomId: String): Unit = {
    rooms += roomId
    messageBus.subscribe(self, RoomsPrefix + roomId)
    usersDBActor ! JoinToRoom(userId, roomId)
  }

  private def leaveRoom(roomId: String): Unit = {
    rooms -= roomId
    messageBus.unsubscribe(self, RoomsPrefix + roomId)
    usersDBActor ! LeaveRoom(userId, roomId)
  }

  private def sendRoomsToClients(): Unit = {
    sendToClients(JsObject(Map(
      "type" -> "rooms".toJson,
      "rooms" -> rooms.toSet.toJson
    )).toString)
  }

  private def sendOldMessagesToClients(): Unit = {
    val messagesFuture = Future.sequence {
      rooms.map { room => messagesDBActor ? GetMessagesOfRoom(room) }.toSet +
        messagesDBActor ? GetMessagesOfUser(userId)
    }
    messagesFuture.onSuccess {
      case messages: Set[_] =>
        sendToClients(JsObject(Map(
          "type" -> "messages".toJson,
          "messages" -> messages
            .flatMap(_.asInstanceOf[Seq[ChatMessage]])
            .toVector
            .sortWith(_.datetime > _.datetime)
            .toJson
        )).toString)
    }
  }
}
