package com.edu.chat.database

import akka.actor.{ActorRef, Props}
import com.edu.chat.database.UsersDBActor._
import com.edu.chat.util.Conversions._
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.model.{Filters, FindOneAndUpdateOptions, Projections, Updates}
import org.mongodb.scala.result.UpdateResult

import scala.collection.JavaConversions._

object UsersDBActor {

  def props(host: String,
            port: Int,
            username: String,
            password: String,
            db: String): Props = Props(new UsersDBActor(host, port, username, password, db))

  case class SaveUser(userId: String, password: String)
  case class GetUser(userId: String)
  case class CheckAuth(userId: String, password: String)
  case object NotFound
  case object Authenticated
  case class JoinToRoom(userId: String, roomId: String)
  case class LeaveRoom(userId: String, roomId: String)
  case class GetRoomsOfUser(userId: String)
}

class UsersDBActor(host: String,
                   port: Int,
                   username: String,
                   password: String,
                   db: String) extends DatabaseActor(host, port, username, password, db) {
  protected override val collectionName = "Users"
  private val collection = loadCollection

  override def receive: Receive = {
    case SaveUser(userId, userPwd) =>
      saveUser(userId, userPwd)(sender)

    case GetUser(userId) =>
      extractUser(userId)(sender)

    case CheckAuth(userId, userPwd) =>
      checkAuth(userId, userPwd)(sender)

    case JoinToRoom(userId, roomId) =>
      joinToRoom(userId, roomId)(sender)

    case LeaveRoom(userId, roomId) =>
      leaveRoom(userId, roomId)(sender)

    case GetRoomsOfUser(userId) =>
      extractRoomsOfUser(userId)(sender)
  }

  private def saveUser(userId: String, userPwd: String)(sender: ActorRef): Unit = {
    collection
      .replaceOne(
        Filters.eq("userId", userId),
        Document(
          "userId" -> userId,
          "password" -> userPwd
        )
      ).subscribe(
      (_: UpdateResult) => sender ! akka.Done,
      (ex: Throwable) => log.error(ex, s"Can't save user with id: $userId, pass: $userPwd")
    )
  }

  private def extractUser(userId: String)(sender: ActorRef): Unit = {
    collection
      .find(Filters.eq("userId", userId))
      .first()
      .collect()
      .subscribe(
        (results: Seq[Document]) =>
          results.headOption match {
            case Some(doc) => sender ! doc.asUser
            case None => sender ! NotFound
          },
        (ex: Throwable) => log.error(ex, s"Can't get user with id: $userId")
      )
  }

  private def checkAuth(userId: String, userPwd: String)(sender: ActorRef): Unit = {
    collection
      .find(
        Filters.and(
          Filters.eq("userId", userId),
          Filters.eq("password", userPwd)))
      .first()
      .collect()
      .subscribe(
        (results: Seq[Document]) =>
          results.headOption match {
            case Some(doc) =>
              sender ! Authenticated
            case None => sender ! NotFound
          },
        (ex: Throwable) => log.error(ex, s"Can't get user with id: $userId")
      )
  }

  private def joinToRoom(userId: String, roomId: String)(sender: ActorRef): Unit = {
    collection
      .findOneAndUpdate(
        Filters.eq("userId", userId),
        Updates.push("rooms", roomId),
        FindOneAndUpdateOptions().upsert(false)
      ).subscribe(
      (_: Document) => sender ! akka.Done,
      (ex: Throwable) => log.error(ex, s"Can't join to the room. User: $userId, room: $roomId")
    )
  }

  private def leaveRoom(userId: String, roomId: String)(sender: ActorRef): Unit = {
    collection
      .findOneAndUpdate(
        Filters.eq("userId", userId),
        Updates.pull("rooms", roomId),
        FindOneAndUpdateOptions().upsert(false)
      ).subscribe(
      (_: Document) => sender ! akka.Done,
      (ex: Throwable) => log.error(ex, s"Can't leave the room. User: $userId, room: $roomId")
    )
  }

  private def extractRoomsOfUser(userId: String)(sender: ActorRef): Unit = {
    collection.find(Filters.eq("userId", userId))
      .projection(Projections.include("rooms"))
      .collect()
      .subscribe(
        (results: Seq[Document]) => results.headOption match {
          case Some(doc) => sender ! doc("rooms").asArray().getValues.map(_.asString().getValue).toList
          case None => sender ! NotFound
        },
        (ex: Throwable) => log.error(ex, s"Can't get rooms of user: $userId"))
  }
}
