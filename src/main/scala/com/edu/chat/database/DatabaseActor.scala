package com.edu.chat.database

import akka.actor.{Actor, ActorLogging}
import com.mongodb.ServerAddress
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.connection.ClusterSettings
import org.mongodb.scala.{MongoClient, MongoClientSettings, MongoCollection, MongoCredential, MongoDatabase}

import scala.collection.JavaConversions._

abstract class DatabaseActor(private val host: String,
                             private val port: Int,
                             private val username: String,
                             private val password: String,
                             private val db: String) extends Actor with ActorLogging {
  protected val collectionName: String

  protected def loadCollection: MongoCollection[Document] = {
    val clusterSettings = ClusterSettings
      .builder()
      .hosts(List(new ServerAddress(host, port)))
      .build()

    val credentials = List(MongoCredential.createCredential(username, db, password.toCharArray))

    val settings = MongoClientSettings
      .builder()
      .clusterSettings(clusterSettings)
      .credentialList(credentials)
      .build()

    val mongoDbClient = MongoClient(settings)

    val database: MongoDatabase = mongoDbClient.getDatabase(db)
    database.getCollection(collectionName)
  }
}
