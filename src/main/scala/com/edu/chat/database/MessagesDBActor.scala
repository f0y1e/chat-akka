package com.edu.chat.database

import java.util.Date

import akka.actor.{ActorRef, Props}
import com.edu.chat.database.MessagesDBActor.{GetMessagesOfRoom, GetMessagesOfUser, SaveMessage}
import com.edu.chat.util.Conversions.DocumentConversions
import com.edu.chat.web.WebSocketConnectionActor.ChatMessage
import org.mongodb.scala.Completed
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.model.{Filters, Projections, Sorts}

object MessagesDBActor {

  def props(host: String,
            port: Int,
            username: String,
            password: String,
            db: String): Props = Props(new MessagesDBActor(host, port, username, password, db))

  case class SaveMessage(chatMessage: ChatMessage)
  case class GetMessagesOfRoom(roomId: String)
  case class GetMessagesOfUser(userId: String)
}

class MessagesDBActor(host: String,
                      port: Int,
                      username: String,
                      password: String,
                      db: String
                     ) extends DatabaseActor(host, port, username, password, db) {
  protected override val collectionName = "Messages"
  private val collection = loadCollection

  override def receive: Receive = {
    case SaveMessage(ChatMessage(from, to, payload, datetime)) =>
      saveMessage(from, to, payload, datetime)(sender)

    case GetMessagesOfRoom(roomId) =>
      extractMessagesOfRoom(roomId)(sender)

    case GetMessagesOfUser(userId) =>
      extractMessagesOfUser(userId)(sender)
  }

  private def saveMessage(from: String, to: String, payload: String, dateTime: Long)(sender: ActorRef): Unit = {
    collection
      .insertOne(Document(
        "from" -> from,
        "to" -> to,
        "payload" -> payload,
        "datetime" -> new Date(dateTime))
      ).subscribe(
      (result: Completed) => sender ! akka.Done
    )
  }

  private def extractMessagesOfRoom(roomId: String)(sender: ActorRef): Unit = {
    collection.find(Filters.eq("to", roomId))
      .projection(Projections.excludeId())
      .sort(Sorts.descending("datetime"))
      .collect()
      .subscribe(
        (results: Seq[Document]) => sender ! results.map(_.asMessage),
        (ex: Throwable) => log.error(ex, s"Can't get messages of room: $roomId"))
  }

  private def extractMessagesOfUser(userId: String)(sender: ActorRef): Unit = {
    collection
      .find(Filters.or(Filters.eq("to", userId), Filters.eq("from", userId)))
      .projection(Projections.excludeId())
      .sort(Sorts.descending("datetime"))
      .collect()
      .subscribe(
        (results: Seq[Document]) => sender ! results.map(_.asMessage),
        (ex: Throwable) => log.error(ex, s"Can't get messages of user: $userId"))
  }
}
